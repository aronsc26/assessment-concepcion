import { Row, Col, Card, Button } from "react-bootstrap";

import styled from "styled-components";

export default function Highlights() {
  const Image = styled.img`
    height: 250px;
    width: 100%;
    border-radius: 20px;
    padding: 0px 0px;
    margin: 0px 0px;
  `;
  return (
    <Row className="row1 mt-3 mb-3">
      <Col className="mb-3" xs={12} md={4}>
        <Card style={{ width: "31rem" }}>
          <Card.Body>
            <Card.Title>
              <Image src="https://cms.recordedfuture.com/uploads/product_threats_ransomware_header_f3e3ea6261.jpg" />
              <h2 className="p-1 m-1">Ransomware</h2>
            </Card.Title>
            <Card.Text style={{ height: "16rem" }}>
              Ransomware methods continue to evolve and multiply, with tactics,
              techniques, and procedures (TTPs) changing constantly. Recorded
              Future proactively keeps tabs on these trends, providing
              intelligence to help you proactively detect, hunt, and monitor
              ransomware attacks.
            </Card.Text>
          </Card.Body>
          <Button
            variant="primary "
            onClick={() =>
              window.open(
                "https://cms.recordedfuture.com/uploads/product_threats_ransomware_header_f3e3ea6261.jpg",
                "_blank"
              )
            }
          >
            Learn More
          </Button>
        </Card>
      </Col>
      <Col className="mb-3" xs={12} md={4}>
        <Card style={{ width: "31rem" }}>
          <Card.Body>
            <Card.Title>
              <Image src="https://cms.recordedfuture.com/uploads/platform_threats_state_sponsored_bc5ba64140.jpg?w=1920" />
              <h2 className="p-1 m-1">State-Sponsored Attacks</h2>
            </Card.Title>
            <Card.Text style={{ height: "16rem" }}>
              State-sponsored cyber actors aggressively target networks in order
              to conduct espionage and compromise, steal, alter, or destroy
              information. These actors go to great lengths to remain
              undetected. Intelligence brings them out of the shadows, providing
              insight into who they are, how they operate, and how they can be
              stopped.
            </Card.Text>
          </Card.Body>
          <Button
            variant="primary"
            onClick={() =>
              window.open(
                "https://cms.recordedfuture.com/uploads/platform_threats_state_sponsored_bc5ba64140.jpg?w=1920",
                "_blank"
              )
            }
          >
            Learn More
          </Button>
        </Card>
      </Col>
      <Col className="mb-3" xs={12} md={4}>
        <Card style={{ width: "31rem" }}>
          <Card.Body>
            <Card.Title>
              <Image src="https://cms.recordedfuture.com/uploads/platform_threats_phishing_75e06a3d2f.jpg?w=1920" />
              <h2 className="p-1 m-1">Phishing</h2>
            </Card.Title>
            <Card.Text style={{ height: "16rem" }}>
              Millions of phishing emails are sent every day, targeting your
              employees and customers. Human error opens the door for attackers
              to gain network access, and move freely while exfiltrating
              critical data. Intelligence helps you track phishing campaigns,
              and the infrastructure used to deliver malware, allowing you to
              accelerate your investigations and defend against future attacks.
              Lorem Ipsum.
            </Card.Text>
          </Card.Body>
          <Button
            variant="primary"
            onClick={() =>
              window.open(
                "https://cms.recordedfuture.com/uploads/platform_threats_phishing_75e06a3d2f.jpg?w=1920",
                "_blank"
              )
            }
          >
            Learn More
          </Button>
        </Card>
      </Col>
      <Col className="mb-3" xs={12} md={4}>
        <Card style={{ width: "31rem" }}>
          <Card.Body>
            <Card.Title>
              <Image src="https://cms.recordedfuture.com/uploads/platform_threats_supply_third_party_ee98a81ac8.jpg?w=1920" />
              <h2 className="p-1 m-1">Supply and Third-Party Risk</h2>
            </Card.Title>
            <Card.Text style={{ height: "16rem" }}>
              Adversaries are continuously targeting third parties, suppliers,
              and vendors to launch software supply chain attacks, disrupt
              physical facilities, deploy ransomware, and more. Intelligence
              helps you to quickly identify and proactively mitigate supply
              chain risk before the damage is done.
            </Card.Text>
          </Card.Body>
          <Button
            variant="primary"
            onClick={() =>
              window.open(
                "https://cms.recordedfuture.com/uploads/platform_threats_supply_third_party_ee98a81ac8.jpg?w=1920",
                "_blank"
              )
            }
          >
            Learn More
          </Button>
        </Card>
      </Col>
      <Col className="mb-3" xs={12} md={4}>
        <Card style={{ width: "31rem" }}>
          <Card.Body>
            <Card.Title>
              <Image src="https://cms.recordedfuture.com/uploads/platform_threats_dark_web_monitoring_5f3d9aa99d.jpg?w=1920" />
              <h2 className="p-1 m-1">Dark Web Monitoring</h2>
            </Card.Title>
            <Card.Text style={{ height: "16rem" }}>
              The dark web is a marketplace for emerging cyber threats, and a
              rich source of intelligence that is often relevant to a broad
              spectrum of potential targets. Our machine learning and natural
              language processing instantly creates links from sites on the dark
              web to other threat sources, enabling you to more quickly
              identify, profile, and mitigate risks to your organization.
            </Card.Text>
          </Card.Body>
          <Button
            variant="primary"
            onClick={() =>
              window.open(
                "https://cms.recordedfuture.com/uploads/platform_threats_dark_web_monitoring_5f3d9aa99d.jpg?w=1920",
                "_blank"
              )
            }
          >
            Learn More
          </Button>
        </Card>
      </Col>
      <Col className="mb-3" xs={12} md={4}>
        <Card style={{ width: "31rem" }}>
          <Card.Body>
            <Card.Title>
              <Image src="https://cms.recordedfuture.com/uploads/credit_card_data_security_a412883ed4.jpg?w=1920" />
              <h2 className="p-1 m-1">Payment Fraud</h2>
            </Card.Title>
            <Card.Text style={{ height: "16rem" }}>
              Criminals leverage a number of nefarious methods to infiltrate
              online e-commerce sites and in-store POS systems to steal payment
              card information. From injecting malicious scripts into Google Tag
              Manager to creating fake payment forms, the tactics criminals use
              are constantly evolving. Recorded Future proactively proactively
              detects attacker infrastructure, compromised points of purchase,
              and stolen card data to prevent fraud before it occurs. Lorem
              Ipsum.
            </Card.Text>
          </Card.Body>
          <Button
            variant="primary"
            onClick={() =>
              window.open(
                "https://cms.recordedfuture.com/uploads/credit_card_data_security_a412883ed4.jpg?w=1920",
                "_blank"
              )
            }
          >
            Learn More
          </Button>
        </Card>
      </Col>
    </Row>
  );
}
