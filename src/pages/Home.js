import { Fragment } from "react";

import Highlights from "../components/Highlights";

export default function Home() {
  return (
    <Fragment>
      <Highlights />
    </Fragment>
  );
}
